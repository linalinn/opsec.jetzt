FROM node:lts-bullseye-slim AS builder-frontend
WORKDIR /usr/webcrypto
COPY package.json package-lock.json ./
RUN npm install
COPY . .
RUN npm run build --prod

FROM nginx:1.21.4-alpine
COPY --from=builder-frontend /usr/webcrypto/dist/opsecJetzt /usr/share/nginx/html
