import { TestBed } from '@angular/core/testing';

import { AsymmetricKeyService } from './asymmetric-key.service';

describe('AsymetricKeyService', () => {
  let service: AsymmetricKeyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AsymmetricKeyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
