import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PasswordDerivedAesKeyComponent } from './password-derived-aes-key.component';

describe('PasswordDerivedAeskeyComponent', () => {
  let component: PasswordDerivedAesKeyComponent;
  let fixture: ComponentFixture<PasswordDerivedAesKeyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PasswordDerivedAesKeyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PasswordDerivedAesKeyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
