import { TestBed } from '@angular/core/testing';

import { UserDataCryptoService } from './user-data-crypto.service';

describe('UserDataCryptoService', () => {
  let service: UserDataCryptoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UserDataCryptoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
